#import vtk
from osgeo import gdal
from osgeo import osr, ogr
import time
import numpy as np

onid=1
import math, sys
import os
import DEMOctTree as tree

def getRotationMatrix(y, p, r):
	#print y
	cosa = math.cos(math.radians(y))
	sina = math.sin(math.radians(y))

	cosb = math.cos(math.radians(p))
	sinb = math.sin(math.radians(p))

	cosy = math.cos(math.radians(r))
	siny = math.sin(math.radians(r))

	r = np.matrix([[cosa*cosb, cosa*sinb*siny-sina*cosy, cosa*sinb*cosy+sina*siny],
				  [sina*cosb, sina*sinb*siny+cosa*cosy, sina*sinb*cosy-cosa*siny],
				  [-1.0*sinb, cosb*siny, cosb*cosy]])
	return r




class View():
	def __init__(self, **kwargs):
		self.obsLat = kwargs.get("obsLat", 0.0)
		self.obsLon = kwargs.get("obsLon", 0.0)
		self.obsElev = kwargs.get("obsElev", 0.0)
		self.horizAngle = kwargs.get("horizAngle", 0.0)
		self.aspectRatio = kwargs.get("aspectRatio", 4/3.0)
		
		
		self.elevFile = kwargs.get("elevFile", "")
		self.geoSr = osr.SpatialReference()
		self.geoSr.ImportFromEPSG(4326)
		self.lccSR = osr.SpatialReference()
		doDEMProj = kwargs.get("projectDEM", True)
		#self.lccSR.ImportFromProj4('+proj=lcc +lon_0=-97 +lat_0=40 +lat_1=33 +lat_2=45 +x_0=0.0 +y_0=0.0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
		self.lccSR.ImportFromProj4('+proj=aeqd  +lat_0=%0.12f +lon_0=%0.12f +units=m'%(self.obsLat, self.obsLon))

		self.transform = osr.CoordinateTransformation(self.geoSr, self.lccSR)
		self.transformBack = osr.CoordinateTransformation(self.lccSR, self.geoSr)


		self.obsPoint = ogr.Geometry(ogr.wkbPoint)
		self.obsPoint.AddPoint( self.obsLon, self.obsLat, self.obsElev	)
		self.obsPoint.Transform(self.transform)
		self.obsPointTuple = [self.obsPoint.GetX(), self.obsPoint.GetY(), self.obsPoint.GetZ()]
		self.obsPointMatrix = np.matrix([[self.obsPointTuple[0]],[self.obsPointTuple[1]],[self.obsPointTuple[2]]])
		self.obsHorizon = self.computeHorizonDistance(self.obsElev)
		self.obsRadius= 6378137+self.obsElev
		self.obsRadiusSq = self.obsRadius**2
		self.eye = self.obsPointMatrix


		if kwargs.has_key("heading") and kwargs.has_key("elevationAngle"):
			#compute view from heading and elevation angles
			self.heading = kwargs.get("heading", 0.0)
			self.elevationAngle = kwargs.get("elevationAngle", 0.0)
			self.up = np.matrix([[0],[0],[1]])
			self.up = getRotationMatrix(0.0,0.0,self.elevationAngle)*self.up#tip the upvector back
			self.up = getRotationMatrix(-1.0*self.heading,0,0)*self.up#rotate the up vector the the correct heading

			self.viewVector = np.matrix([[0],[1],[0]])
			self.viewVector = getRotationMatrix(-1.0*self.heading, 0.0, self.elevationAngle)*self.viewVector
			self.n = self.viewVector*-1 #Normal vector of viewing plane
			
		elif kwargs.has_key("viewVector") and kwargs.has_key("upVector"):
			self.up = kwargs.get("upVector")
			self.up = np.matrix([[self.up[0]], [self.up[1]], [self.up[2]]])
			self.viewVector = kwargs.get("viewVector")
			self.viewVector = np.matrix([[self.viewVector[0]], [self.viewVector[1]], [self.viewVector[2]]])



		self.n = self.viewVector*-1 #Normal vector of viewing plane
		up2 = [self.up[0][0].item(),self.up[1][0].item(), self.up[2][0].item()]
		n2 = [self.n[0][0].item(), self.n[1][0].item(), self.n[2][0].item()]
		self.u = np.cross(up2,n2)

		self.u = np.matrix([[self.u[0]], [self.u[1]], [self.u[2]]])
		self.u = self.u / np.linalg.norm(self.u)

		self.v = np.cross([self.n[0][0].item(), self.n[1][0].item(), self.n[2][0].item()],[self.u[0][0].item(), self.u[1][0].item(), self.u[2][0].item()])
		self.v = np.matrix([[self.v[0]], [self.v[1]], [self.v[2]]])
		self.H = 100
		self.W = self.H*self.aspectRatio
		self.d = float(self.W) / (2*math.tan(math.radians(self.horizAngle / 2.0)))
		self.C = self.eye - self.n * self.d #view plane center
		self.L = self.C - self.u *self.W/2.0 - self.v*self.H/2.0
		self.planeD = -1.0* np.dot(np.squeeze(np.asarray(self.C)), np.squeeze(np.asarray(self.n)))

		print ("Loading DEM")
		t0 = time.time()
		print ("DEM Loaded %0.12f"%(time.time() - t0))

		if doDEMProj:
			self.projectDEM()
		else:
			self.geo = gdal.Open(self.elevFile)

		(upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = self.geo.GetGeoTransform()
		self.geoT = {"upper_left_x":upper_left_x, "x_size":x_size, "upper_left_y":upper_left_y, "y_size":y_size, "x_pix": self.geo.RasterXSize, "y_pix":self.geo.RasterYSize}	
		self.matrix = self.geo.ReadAsArray()
		
		self.bbtree = tree.bbox(self.matrix, 0, self.geo.RasterYSize-1, 0, self.geo.RasterXSize-1, self.geoT, self.transform, self.transformBack, self.obsPointTuple, maxDepth=8)
		self.iterCount = 0
		self.ptCount = 0
		print "File Loaded"
		print ("Loading DEM matrix")
		t0 = time.time()
		print ("DEM Loaded %0.12f"%(time.time() - t0))

	def projectDEM(self):
		t0 = time.time()
		pixel_spacing = 10.0
		print "Reprojecting DEM"
		geoIn = gdal.Open(self.elevFile)
		(upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = geoIn.GetGeoTransform()

		self.geoT = {"upper_left_x":upper_left_x, "x_size":x_size, "upper_left_y":upper_left_y, "y_size":y_size, "x_pix": geoIn.RasterXSize, "y_pix":geoIn.RasterYSize}


		#transform the DEM to destination coordiate system
		(ulx, uly, ulz) = self.transform.TransformPoint(upper_left_x, upper_left_y)
		(lrx, lry, lrz) = self.transform.TransformPoint(upper_left_x + x_size*geoIn.RasterXSize, upper_left_y + y_size*geoIn.RasterYSize)

		mem_drv = gdal.GetDriverByName('MEM')
		dest = mem_drv.Create('', int((lrx-ulx)/pixel_spacing), int((uly-lry)/pixel_spacing), 1, gdal.GDT_Float32)
		new_geo = (ulx, pixel_spacing, x_rotation, uly, y_rotation, -pixel_spacing)
		dest.SetGeoTransform(new_geo)
		dest.SetProjection(self.lccSR.ExportToWkt())
		res = gdal.ReprojectImage(geoIn,dest, self.geoSr.ExportToWkt(), self.lccSR.ExportToWkt(), gdal.GRA_Bilinear)
		self.geo = dest
		
		print "Reprojection complete %0.12f"%(time.time() - t0)
		



	def computeHorizonDistance(self, elev):
		return (elev*(12756274.0+elev))**0.5

	def divideGrid(self, g):
		"""
			Divides matrix g of shape (r, c) into a new grid of shape (r*2-1, c*2-1)
			Cell (i,j) in g becomes cell (i*2, j*2) in the divided grid.
			cells above the skyline will have the value nan, including newly introduced rows
		"""
		assert (g.shape[0] %2 == 1)
		assert (g.shape[1] % 2 == 1)
		#new grid shape is computed to insert new rows/cols between those of g.  Grid edges and central vector are presevered
		newShape = (g.shape[0] * 2 -1, g.shape[1] * 2 -1)
		cols = newShape[0]
		rows = newShape[1]
		ranges = np.zeros((rows, cols))

		oldRows = g.shape[0]
		oldCols = g.shape[1]

		for i in range(oldRows):
			for j in range(oldCols):
				if not np.isnan(g[i][j]):
					ranges[i*2][j*2] = g[i][j]
				else:
					#if the old cells below, over and diagonally below and over are all nan, then this cell and the adjacent cells can be nan
					if (j+1 < oldCols and i+1 < oldRows):
						if np.isnan(g[i][j+1]) and np.isnan(g[i+1][j]) and np.isnan(g[i+1][j+1]):
							ranges[i*2][j*2+1] = np.nan
							ranges[i*2+1][j*2] = np.nan
							ranges[i*2][j*2] = np.nan
							ranges[i*2+1][j*2+1] = np.nan
		return ranges


	def computeRay(self, i, j, planeShape):
		"""
			Computes a ray through the viewing plane of this view object.
			i: image row (0 is bottom row of image)
			j: image column (0 is left column of image)
			planeShape: tuple describing the pixel (height, width) of the view plane
			returns: ray with origin at camera eye and direction through the given pixel of the viewing plane
		"""
		wx = self.W / float(planeShape[1])
		hy = self.H / float(planeShape[0])
		planeVector = self.L + self.u*(j+0.5)*wx + self.v*(i+0.5)*hy
		d2 = planeVector-self.eye #obsRotMatrix*viewVector + p0m
		d2 = d2 / np.linalg.norm(d2)
		r = tree.ray(np.squeeze(np.asarray(self.eye)), np.squeeze(np.asarray(d2)))
		return r

	def computeViewPlanePixel(self, ray, planeShape):
		"""
			Computes the viewplane pixel for a given ray and view plane matrix shape
			ray: The ray for which to compute the pixel within the view plane
			planeShape: tuple describing the shape of the view plane in pixels (height, width)
			returns: Tuple (column, row) for give plane pixel intersected by the ray.  will be none if the ray does not intersect the view plane
		"""
		e = np.squeeze(np.asarray(self.eye))
		n = np.squeeze(np.asarray(self.n))
		v = np.array(ray.viewVector)
		wx = self.W / float(planeShape[1])
		hy = self.H / float(planeShape[0])

		t = -1*(np.dot(e,n)+self.planeD) / np.dot(v,n)
		P = ray.getPoint(t)
		V = np.squeeze(np.asarray(self.v))
		U = np.squeeze(np.asarray(self.u))
		L = np.squeeze(np.asarray(self.L))

		j = np.dot(V, (P-L)) / (np.linalg.norm(V)*hy)
		i = np.dot(U, (P-L)) / (np.linalg.norm(U)*wx)

		if i < 0 or i >= planeShape[1] or np.isnan(i):
			i = None
		else:
			i = int(math.floor(i))

		if j < 0 or j >= planeShape[0] or np.isnan(j):
			j = None
		else:
			j = int(math.floor(j))

		return (i,j)



	def doSkyline(self, ranges, elevations=None):
		#prepare skyline export
		#driver = ogr.GetDriverByName('ESRI Shapefile')
		shapeData = None
		# if os.path.exists('skyline.shp'):
		# 	files = os.listdir('./')
		# 	for f in files:
		# 		if f.startswith('skyline.'):
		# 			os.remove(f)


		# shapeData = driver.CreateDataSource('skyline.shp')
		# layer = shapeData.CreateLayer('Skyline', self.geoSr, ogr.wkbPoint)
		# layerDef = layer.GetLayerDefn()
		# skyline = ogr.Geometry(ogr.wkbPoint)


		print "Seeking skyline"
		cols = ranges.shape[1]
		rows = ranges.shape[0]
		
		
		pSource = self.obsPointTuple

		#starting at the left top, move down to find the row where the skyline starts
		skyEnd = [0,0]		
		j = 0 #left column
		for i in range(rows-1, -1, -1): #move top to bottom
			if ranges[i][j] == 0: # need to compute the range
				r = self.computeRay(i,j, ranges.shape)
				pt = self.bbtree.findIntersection(r)

				if pt != None: 
					#ptgg = ogr.Geometry(ogr.wkbPoint)
					#ptgg.AddPoint(pt['point'][0], pt['point'][1])
					#ptgg.Transform(self.transformBack)
					#skyline.AddGeometry(ptgg)
					ranges[i][j] = pt['dist']
					if elevations is not None:
						elevations[i][j] = pt['meshElev']
				else: #no intersection, set to nan
					ranges[i][j] = np.nan
					if elevations is not None:
						elevations[i][j] = np.nan
			if not np.isnan(ranges[i][j]): #skyline located
				skyEnd = [i,j]
				print "Skyline located at %d %d  range %0.2f"%(skyEnd[0], skyEnd[1], ranges[i][j].item())
				break
		print "Tracing Skyline\n"
		#next trace across
		i = skyEnd[0]
		j = skyEnd[1]+1
		seekGround = True
		while j < ranges.shape[1] and i < ranges.shape[0]: #move across
			sys.stdout.write("\r%3d%%"%(int((j)/float(cols-1)*100)))
			sys.stdout.flush()
			if i > ranges.shape[0]: #never found the sky
				print "Never found the sky"
				seekGround=True
				i=ranges.shape[0]-1
				j+=1
				continue
			if i< 0: #never found the ground
				print "Never found ground"
				seekGround = True
				i=0
				j+=1
				continue
			if ranges[i][j] == 0: # need to compute the range
				r = self.computeRay(i,j, ranges.shape)
				
				
				pt = self.bbtree.findIntersection(r)
				if pt != None: 
					#ptgg = ogr.Geometry(ogr.wkbPoint)
					#ptgg.AddPoint(pt['point'][0], pt['point'][1])
					#ptgg.Transform(self.transformBack)
					#skyline.AddGeometry(ptgg)
					ranges[i][j] = pt['dist']
					if elevations is not None:
						elevations[i][j] = pt['meshElev']
				else: #no intersection, set to nan
					ranges[i][j] = np.nan
			if np.isnan(ranges[i][j]): #this is sky
				if seekGround: #move down and try again
					i-=1 #move down
					continue #keep looking for ground
				else: #we are looking for sky and found it
					seekGround = True # switch to sky searc
					i -= 1 #move down to the ground again???
					continue
			else: #this is ground
				if seekGround: #found the ground we were looking for, all above are sky
					ranges[i+1:rows, j] = np.nan
					if elevations is not None:
						elevations[i+1:rows, j] = np.nan
					j += 1 # move over
					seekGround = False
					continue
				else: # looking for sky, move up
					i += 1
					continue
		print ""
		print "Done with skyline"
		#Finish skyline point export
		#featureIndex=0
		#feature = ogr.Feature(layerDef)
		#feature.SetGeometry(skyline)
		#feature.SetFID(featureIndex)
		#layer.CreateFeature(feature)
		#shapeData.Destroy()
		return ranges, elevations




	def computeRanges(self, prevGrid=None, prevElevs = None, divideGrid=False, arrayDumpName=None, elevDumpName=None):
		"""
			p0 - Observation point
			d - scalar distance for max ray length
			arr - DEM

		"""
		#Changes to implement:
		#	Don't use delta angle, instead use the code here to compute view vectors: http://www.unknownroad.com/rtfm/graphics/rt_eyerays.html
		#	1. Compute tan(fovx) and tan(fovy)
		#	2. Iterate pixels (u,v)
		#		a. Compute vectors for u/v using tangents
		#		b. Rotate computed vectors per camera heading, tilt, (roll?)
		#		c. Translate vectors to camera position
		#	3. Separate skyline finding, this may not be necessary when seeded with a lower res matrix
		#	4. Implement range reduction to save on intersection computation
		#		Begin with max range until skyline is found
		#		Trace downward using range of pixel above plus some added buffer range, hopefully yields only one intersection

		elevs = None
		#Start with the Eye and coordinate of interest
		if divideGrid:
			ranges = self.divideGrid(prevGrid)
			if prevElevs is not None:
				elevs = self.divideGrid(elevs)
		else:
			ranges = prevGrid
			if prevElevs is not None:
				elevs = prevElevs
		
		rows = ranges.shape[0]
		cols = ranges.shape[1]

		
		wx = self.W / float(cols)
		hy = self.H / float(rows)

		pSource = self.obsPointTuple
		p0m = self.obsPointMatrix
		#new grid shape is computed to insert new rows/cols between those of prevGrid.  Grid edges and central vector are presevered
		ranges, elevs = self.doSkyline(ranges, elevs)
		print "Computing Ranges"
		print ""
		for j in range(cols): #left to right
			sys.stdout.write("\r%3d%%"%(int((j)/float(cols-1)*100)))
			sys.stdout.flush()

			for i in range(rows-1,-1,-1):#top to bottom
				if ranges[i][j] == 0 and not np.isnan(ranges[i][j]):
					r = self.computeRay(i,j, ranges.shape)
					pt = self.bbtree.findIntersection(r)

					if pt != None: 
						ranges[i][j] = pt['dist']
						if elevs is not None:
							elevs[i][j] = pt['meshElev']
					else: #no intersection, set to nan
						ranges[i][j] = np.nan
						if elevs is not None:
							elevs[i][j] = np.nan

			if (j % 100 == 0 and arrayDumpName is not None): 
				np.save(arrayDumpName, ranges)
				if elevs is not None:
					np.save(elevDumpName,elevs)
		ranges[ ranges <= 0.0 ] = np.nan
		np.save(arrayDumpName, ranges)
		if elevs is not None: np.save(elevDumpName, elevs)
		sys.stdout.write("\nDone With Ranges\n")
		sys.stdout.flush()
		return ranges, elevs


	def computeElevationOffset(self, rng):
		yoff = self.obsRadius - (self.obsRadiusSq-rng**2)**0.5
		return yoff

	def curvatureCorrection(self, ranges, elevations):
		"""
			Applies curvature correction to the range/elevation matrices.
			The general process is as follows:
				1. Sweep the image left to right, bottom to top
				2. For each pixel determine if the range is valid based on horizon distance
					range <= (observation point horizon distance) + (observed point horizon distance)
				3. If the horizon validation passes, shift the pixel downward to account for earth curvature.
				   Earth curvature causes a perceived reduction in elevation as a function of distance from the observation point.
		"""
		eye = np.squeeze(np.asarray(self.eye))
		#May need to use a 1/0 matrix to track which cells are available for rewrite, and leave the original value in ranges/elevations in order to not get any holes
		for j in range(ranges.shape[0]): #bottom to top
			sys.stdout.write("\r%3d%%"%(int((j)/float(ranges.shape[0]-1)*100)))
			sys.stdout.flush()
			for i in range(ranges.shape[1]): #left to right
				# check the horizon ranges
				rng = ranges[j][i]
				elev = elevations[j][i]
				h = self.computeHorizonDistance(elev)

				if rng > h+self.obsHorizon:
					#this is an invalid point, discard it
					ranges[j][i] = np.nan
					elevations[j][i] = np.nan
					continue

				#range is validated by horizon distance, compute pixel shift
				r = self.computeRay(j, i, ranges.shape) # get the ray for this pixel
				p = r.getPoint(rng) #get the point on this ray at the known range
				offset = self.computeElevationOffset(rng)#compute the elevation offset due to curvature at the known range
				p[2] -= offset #apply the elevation offset
				pa = np.array(p)
				v = pa - eye#compute a vector from eye to offset point p
				v = v / np.linalg.norm(v)# scale to a unit vector
				r2 = tree.ray(r.origin, v) #create a new ray for the offset point
				i2, j2 = self.computeViewPlanePixel(r2, ranges.shape)#compute the pixel coordiates for the offset ray
				if i == i2 and j ==j2: continue#if no shift occurs, continue
				if i2 is None or j2 is None:#if the offset pixel is outside the view plane, discard this range/elev
					ranges[j][i] = np.nan
					elevations[j][i] = np.nan
					continue
				if np.isnan(ranges[j2][i2]) or ranges[j2][i2] > rng: #if the offset pixel is empty or the range occupying the offest pixel is greater than this range, set the range/elevation
					ranges[j2][i2] = rng
					elevations[j2][i2] = elev
					ranges[j][i] = np.nan
					elevations[j][i] = np.nan
					continue
				if ranges[j2][i2] < rng:#if the offset pixel is occupied and the offset pixel range is less than this range, discard this range/elev
					ranges[j][i] = np.nan
					elevations[j][i] = np.nan
					continue
		return (ranges, elevations)




	
if __name__ == "__main__":
	v = View(obsLat=40.596496, obsLon=-105.165740, obsElev=1697, horizAngle=73, vertAngle=54.75, heading=292.19, elevationAngle=-4.8, terrainFile="Test6.stl", maxRange=12000)
	# v = View(obsLat=40.587316, obsLon=-105.152516, obsElev=1597, horizAngle=73, vertAngle=54.75, heading=304, elevationAngle=4.2, terrainFile="Test5.stl", maxRange=12000)
	onid =40
	#prev = np.load("arrayDump2.npy")
	prev = np.zeros((33,33))
	#prev = v.doSkyline(prev)
	#np.save("skydump.npy", prev)
	for i in range(10):
		prev = v.computeRanges(prevGrid=prev)
		onid += 1

