import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import cPickle as pkl 

def showRanges(fname, flipud=False, label="Visible distance (m)"):
	r = np.load(fname)
	if flipud:
		r = np.flipud(r)
	r[r==0] = np.nan
	cdict1 = {'red':   ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0)),

	         'green': ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0)),

	         'blue':  ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0))
	        }
	blue_red1 = colors.LinearSegmentedColormap('BlueRed1',cdict1, N=4096)
	plt.figure(figsize=(20,10))
	plt.imshow(r, interpolation="nearest", origin="lower",aspect='auto')
	cb = plt.colorbar()
	cb.set_label(label)
	plt.show()

def showEdges(fname):
	r = np.load(fname)
	r[ r <= 0.0 ] = np.nan

	cdict1 = {'red':   ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0)),

	         'green': ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0)),

	         'blue':  ((0.0, 0.0, 0.0),
	                   (1.0, 1.0, 1.0))
	        }
	blue_red1 = colors.LinearSegmentedColormap('BlueRed1',cdict1, N=4096)

	plt.imshow(r, interpolation="nearest", origin="lower",aspect='auto')
	cb = plt.colorbar()
	cb.set_label('Visible distance (m)')
	plt.show()

	#gradient close 100,100,100 far 225,225,225