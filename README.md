# README #

This repository contains utilities for generating per pixel range values for a photograph.  The primary purpose for these range masks are to allow photos to be incorporated into the WinHaze haze modeling tool.

### What is this repository for? ###

* Python scripts for generating range masks from digital elevation data.

### How do I get set up? ###

* Install Enthought Python (Or configure a python environment with gdal and pyOpenCV)
* Download the source code
* Set variables in SetVars.bat (Windows only)
* Execute 0_InstallDependencies.bat (Windows only, other platforms just read this file and do what it does)
* Execute 1_CreateProject.bat (Windows only) to create a view match project
* Within the project folder fill in parameters in config.json
* Within the project folder execute each numbered .bat file in sequence

For more information read the [wiki](https://bitbucket.org/iwdw/winhazeviewmatch/wiki/Home "wiki")
### Who do I talk to? ###

* dustinlschmidt@gmail.com

