
import random
import numpy
import math
from osgeo import gdal
from osgeo import osr, ogr
import math, sys
import numpy as np



def idxToPoint(i, j, l, t, x_size, y_size):#convert an grid index to a point
	x = l+i*x_size
	y = t+j*y_size
	return (x+0.5*x_size,y+0.5*y_size)

def pointToIdx(x, y, l, t, x_size, y_size):
	i = (x-l) / x_size
	j = (y-t) / y_size
	i = int(math.floor(i))
	j = int(math.floor(j))
	return (i,j)

def dist(p1,p2):
	return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 + (p1[2]-p2[2])**2)**0.5

def convertCoords(x,y,z, t):#convert some x,y,z coordiates with transform t
	point = ogr.Geometry(ogr.wkbPoint)
	point.AddPoint(x,y)
	point.Transform(t)
	return (point.GetX(), point.GetY(), z)

class ray():
	def __init__(self, origin, viewVector):
		self.origin = origin
		self.viewVector = viewVector
		self.npOrigin = np.array(origin)
		self.npView = np.array([float(viewVector[0]), float(viewVector[1]), float(viewVector[2])])
		self.invView= 1/ self.npView
		self.sign = [self.invView[0] < 0, self.invView[1] < 0, self.invView[2] < 0]

	def getPoint(self, t):
		return self.npOrigin + t*self.npView

class bbox():
	iterCount = 0
	ptCount = 0
	def __init__(self, matrix, rowS, rowE, colS, colE, matrixDetails, transform, transformBack, obsPt, parent=None, depth=0, maxDepth=6):
		self.matrix = matrix

		self.rowS = rowS
		self.rowE = rowE
		self.colS = colS
		self.colE = colE
		self.matrixDetails = matrixDetails
		self.parent = parent
		self.transform = transform
		self.transformBack = transformBack
		self.depth = depth
		self.maxDepth = maxDepth

		self.upper_left_x = self.lower_left_x = self.matrixDetails["upper_left_x"] + colS*self.matrixDetails["x_size"]
		self.upper_right_x = self.lower_right_x = self.matrixDetails["upper_left_x"] + (colE+1)*self.matrixDetails["x_size"]
		self.upper_left_y = self.upper_right_y = self.matrixDetails["upper_left_y"] + rowS*self.matrixDetails["y_size"]
		self.lower_left_y = self.lower_right_y = self.matrixDetails["upper_left_y"] + (rowE+1)*self.matrixDetails["y_size"]
		self.obsPt = obsPt
		#self.fixMatrix(self.obsPt)


		self.subMatrix = self.matrix[rowS:(rowE+1), colS:(colE+1)]
		zmax = self.subMatrix.max()
		zmin = self.subMatrix.min()

		c1 = (self.upper_left_x, self.upper_left_y, zmin)
		c2 = (self.upper_right_x, self.upper_right_y, zmin)
		c3 = (self.lower_right_x, self.lower_right_y, zmin)
		c4 = (self.lower_left_x, self.lower_left_y, zmin)
		c5 = (self.upper_left_x, self.upper_left_y, zmax)
		c6 = (self.upper_right_x, self.upper_right_y, zmax)
		c7 = (self.lower_right_x, self.lower_right_y, zmax)
		c8 = (self.lower_left_x, self.lower_left_y, zmax)


		self.xmin = min(c1[0], c2[0], c3[0], c4[0], c5[0], c6[0], c7[0], c8[0])-0.0000100
		self.xmax = max(c1[0], c2[0], c3[0], c4[0], c5[0], c6[0], c7[0], c8[0])+0.0000100
		self.ymin = min(c1[1], c2[1], c3[1], c4[1], c5[1], c6[1], c7[1], c8[1])-0.0000100
		self.ymax = max(c1[1], c2[1], c3[1], c4[1], c5[1], c6[1], c7[1], c8[1])+0.0000100
		self.zmin = zmin-0.0000100
		self.zmax = zmax+0.0000100

		self.children = []

		if self.depth < maxDepth:
			self.divide()

	def idxToPoint(self, col, row):
		return idxToPoint(col, row, self.matrixDetails['upper_left_x'], self.matrixDetails['upper_left_y'], self.matrixDetails['x_size'], self.matrixDetails['y_size'])

	def pointToIdx(self, x, y):
		return pointToIdx(x, y, self.matrixDetails['upper_left_x'], self.matrixDetails['upper_left_y'], self.matrixDetails['x_size'], self.matrixDetails['y_size'])

	def fixMatrix(self, obsPt):
		print "Beginning Earth Curvature Correction\n"
		for i in range(self.matrix.shape[0]):
			sys.stdout.write("\r%0.8f"%((i)/float(self.matrix.shape[0]-1)))
			sys.stdout.flush()
			for j in range(self.matrix.shape[1]):
				otherPt = idxToPoint(j, i, self.matrixDetails['upper_left_x'], self.matrixDetails['upper_left_y'], self.matrixDetails['x_size'], self.matrixDetails['y_size'])
				otherPt = convertCoords(otherPt[0], otherPt[1], self.matrix[i][j].item(), self.transform)
				dist = ((obsPt[0]-otherPt[0])**2 + (obsPt[1]-otherPt[1])**2)**0.5
				r = 6371000+obsPt[2]
				off = r - (r**2 - dist**2)**0.5
				newZ = otherPt[2] - off
				self.matrix[i][j] = newZ
		print "\nEarth Curvature Correction Complete"


	def divide(self):
		midRow1 = self.rowS+int(math.floor(0.5*(self.rowE-self.rowS)))
		midRow2 = self.rowS+int(math.ceil(0.5*(self.rowE-self.rowS)))
		assert(midRow1 == midRow2 or midRow2 == midRow1 + 1)

		midCol1 = self.colS+int(math.floor(0.5*(self.colE-self.colS)))
		midCol2 = self.colS+int(math.ceil(0.5*(self.colE-self.colS)))
		assert(midCol1 == midCol2 or midCol2 == midCol1 + 1)

		self.children.append(bbox(self.matrix, self.rowS, midRow1, self.colS, midCol1, self.matrixDetails, self.transform, self.transformBack, self.obsPt, parent=self, depth=self.depth+1, maxDepth=self.maxDepth))
		self.children.append(bbox(self.matrix, midRow2, self.rowE, self.colS, midCol1, self.matrixDetails, self.transform, self.transformBack, self.obsPt, parent=self, depth=self.depth+1, maxDepth=self.maxDepth))
		self.children.append(bbox(self.matrix, self.rowS, midRow1, midCol2, self.colE, self.matrixDetails, self.transform, self.transformBack, self.obsPt, parent=self, depth=self.depth+1, maxDepth=self.maxDepth))
		self.children.append(bbox(self.matrix, midRow2, self.rowE, midCol2, self.colE, self.matrixDetails, self.transform, self.transformBack, self.obsPt, parent=self, depth=self.depth+1, maxDepth=self.maxDepth))

	def intersectFromTval(self, ray, tvals):
		p = ray.getPoint(tvals[0])
		idx = self.pointToIdx(p[0], p[1])
		if idx[0] < 0 or idx[0] >= self.matrix.shape[1] or idx[1] < 0 or idx[1] >= self.matrix.shape[0]:
			#print "Bad Index: %s found at tval %0.12f"%(str(idx), tvals[0])
			return None

		else:
			meshElev = self.matrix[idx[1]][idx[0]]
			
			tvalOn = tvals[0]
			if tvalOn < 0: tvalOn=0
			d = None
			count = 0 #track iterations
			pLast = None
			while d is None and tvalOn < tvals[1]+20:
				#if (hitSurface(p[2], meshElev)):
				#	import pdb; pdb.set_trace()
				count += 1
				idx = self.pointToIdx(p[0], p[1])
				
				if (idx[1] < self.matrix.shape[0] and idx[0] < self.matrix.shape[1] and idx[0] >=0 and idx[1] >=0):

					meshElev = self.matrix[idx[1]][idx[0]]
					if p[2] <= meshElev:
						if pLast is None:
							d = tvalOn
						else:
							d = self.planeIntersect(pLast, ray)
					else:
						pLast = np.array([p[0], p[1], meshElev])
				else: 
					break
					

				tvalOn += 5
				p = ray.getPoint(tvalOn)
				

			if d is not None:
				bbox.iterCount += count
				bbox.ptCount += 1
				#compute triangles and intersect with ray
				t = d#tvalOn
				#t=self.intersectTriangles(idx[0], idx[1], ray)
				if t is not None:
					p = ray.getPoint(t)
					pt = {"point":p, "cellCoords":idx, "meshElev":meshElev, "tval":t, "dist":t}
					return pt
			# else:
			# 	print "missed surface pz %0.12f meshz %0.12f"%(p[2], meshElev)
		return None

	def getIters(self):
		return (bbox.iterCount, bbox.ptCount)

	def planeIntersect(self, P0, ray):
		R = np.array(ray.viewVector)
		O = np.array(ray.origin)
		P0 = np.array(P0)
		t = (np.dot(R, P0) - np.dot(R,O)) / np.dot(R,R)
		return t

	def MTIntersect(self, v0,v1,v2,ray):
		#import pdb; pdb.set_trace()
		o = np.array(ray.origin)
		d = np.array(ray.viewVector)

		v0v1 = v1-v0
		v0v2 = v2-v0
		pvec = np.cross(d, v0v2)
		det = np.dot(v0v1, pvec)
		if (math.fabs(det) < 0.001):
			return None

		invDet = 1.0 / det
		tvec = o - v0

		u = np.dot(tvec, pvec)* invDet
		if (u<0 or u > 1):
			return None

		qvec = np.cross(tvec, v0v1)
		v = np.dot(d, qvec) * invDet

		if (v < 0 or v > 1):
			return None

		t = np.dot(v0v2, qvec) * invDet

		return t

	def getVertex(self, i, j):
		p1z = self.matrix[j][i]

		p1x, p1y = self.idxToPoint(i, j)
		v0 = np.array([p1x,p1y,p1z])
		return v0

	def intersectTriangles(self, i, j, ray):

		v0 = self.getVertex(i,j)
		v1 = self.getVertex(i-1, j-1)
		v2 = self.getVertex(i+1, j-1)

		tintersect = self.MTIntersect(v0,v1,v2,ray)
		if tintersect != None:
			return tintersect

		v3 = self.getVertex(i+1,j+1)
		tintersect = self.MTIntersect(v0,v2,v3,ray)
		if tintersect != None:
			return tintersect

		v4 = self.getVertex(i-1, j+-11)
		tintersect = self.MTIntersect(v0,v3,v4,ray)
		if tintersect != None:
			return tintersect

		return tintersect


	def findIntersectBoxes(self, ray, boxes):
		intsct, tvals = self.intersects(ray)
		if intsct:
			if len(self.children) > 0:
				for c in self.children:
					c.findIntersectBoxes(ray, boxes)
			else:
				if (len(tvals) >0):
					boxes.append({'bbox':self, 'tvals':tvals})

	def findIntersection(self, ray):
		boxes = []
		self.findIntersectBoxes(ray,boxes)
		boxes = sorted(boxes, key=lambda x:x['tvals'][0])

		pt = None
		for b in boxes:
			pt = b['bbox'].intersectFromTval(ray, b['tvals'])
			if pt != None:
				return pt
		return pt

	def pointInBox(self, pt):
		if pt[0] > 0 : pt[0] = math.floor(pt[0])
		if pt[0] < 0: pt[0] = math.ceil(pt[0])
		if pt[1] > 0 : pt[1] = math.floor(pt[1])
		if pt[1] < 0: pt[1] = math.ceil(pt[1])
		if pt[2] > 0 : pt[2] = math.floor(pt[2])
		if pt[2] < 0: pt[2] = math.ceil(pt[2])
		inbox= ( pt[0] >= self.xmin) and ( pt[0] <= self.xmax) and ( pt[1] >= self.ymin) and ( pt[1] <= self.ymax) and ( pt[2] >= self.zmin) and ( pt[2] <= self.zmax)
		return inbox


	def intersects(self, ray):
		mins = []
		maxs = []
		txmin = tymin = tzmin= float("inf")
		txmax = tymax = tzmax=-1*float("inf")

		if (ray.viewVector[0] != 0):
			txmin = (self.xmin - ray.origin[0])/ray.viewVector[0]		
			txmax = (self.xmax - ray.origin[0])/ray.viewVector[0]
			if txmin > txmax: txmin, txmax = txmax, txmin
		else:
			if ray.origin[0] <= self.xmax and ray.origin[0] >= self.xmin:
				txmax = -1*txmax
				txmin = -1*txmin

		if (ray.viewVector[1] != 0):
			tymin = (self.ymin - ray.origin[1])/ray.viewVector[1]
			tymax = (self.ymax - ray.origin[1])/ray.viewVector[1]
			if tymin > tymax: tymin, tymax = tymax, tymin
		else:
			if ray.origin[1] <= self.ymax and ray.origin[1] >= self.ymin:
				tymax = -1*tymax
				tymin = -1*tymin

		if (ray.viewVector[2] != 0):
			tzmin = (self.zmin - ray.origin[2])/ray.viewVector[2]
			tzmax = (self.zmax - ray.origin[2])/ray.viewVector[2]
			if tzmin > tzmax: tzmin, tzmax = tzmax, tzmin
		else:
			if ray.origin[2] <= self.zmax and ray.origin[2] >= self.zmin:
				tzmax = -1*tzmax
				tzmin = -1*tzmin

		tmin = max(txmin, tymin, tzmin)
		tmax = min(txmax, tymax, tzmax)

		if (tmax < 0 or tmax < tmin or tmax == -1*float("inf") or tmin == float("inf") or tmax == float("inf") or tmin == -1*float("inf")):
			return (False, [])

		if (tmin < 0 or tmin == None):
			tmin = 0

		return (True, [tmin, tmax])

	def intersectsold(self, ray):
		if ray.viewVector[0] == 0 and ray.viewVector[1] == 0 and ray.viewVector[2] == 0:
			if self.pointInBox(ray.origin):
				return (True, [0])
			else:
				return( False, [])

		txmin = tymin = tzmin = txmax = tymax = tzmax = None

		if ray.viewVector[0] != 0:
			txmin = (self.xmin - ray.origin[0]) / ray.viewVector[0]
			txmax = (self.xmax - ray.origin[0]) / ray.viewVector[0]

		if txmin > txmax: txmin,txmax = txmax, txmin

		if ray.viewVector[1] != 0:
			tymin = (self.ymin - ray.origin[1]) / ray.viewVector[1]
			tymax = (self.ymax - ray.origin[1]) / ray.viewVector[1]

		if tymin > tymax: tymin, tymax = tymax, tymin

		if ray.viewVector[2] != 0:
			tzmin = (self.zmin - ray.origin[2]) / ray.viewVector[2]
			tzmax = (self.zmax - ray.origin[2]) / ray.viewVector[2]

		if (tzmin > tzmax): tzmin, tzmax = tzmax, tzmin

		if (txmin < 0 and txmax < 0) or (tymin < 0 and tymax < 0 ) or (tzmin < 0 and tzmax < 0): #intersection is behind viewpoint, not in direction of vector
			return (False, [])

		

		tval = [txmin,txmax, tymin, tymax, tzmin,tzmax]
		goodTs = []
		for t in tval:
			if t == None: continue
			#if t != -1*float("inf") and t != float("inf"):
			#	goodTs.append(t)
			pt = ray.getPoint(t)
			#pt[0] = round(pt[0],7)
			#pt[1] = round(pt[1],7)
			#pt[2] = round(pt[2],7)
			if self.pointInBox(pt) :
				goodTs.append(t)

		if self.pointInBox(ray.origin):
			goodTs.append(0)

		if len(goodTs) != 2 and len(goodTs) !=0:
			import pdb; #pdb.set_trace()
		if len(goodTs) > 0:
			goodTs.sort()
			return (True, goodTs)
		else:
			return (False, goodTs)

		



if __name__=="__main__":
	elevFile = '/media/sf_D_DRIVE/Projects/ImageRanging/GrandCanyon1/elev/floatn37w113_13.flt'
	obsPt=[-112.117574, 36.066052, 2155]
	geoSr = osr.SpatialReference()
	geoSr.ImportFromEPSG(4326)
	aeqdSR = osr.SpatialReference()
	#lccSR.ImportFromProj4('+proj=lcc +lon_0=-97 +lat_0=40 +lat_1=33 +lat_2=45 +x_0=0.0 +y_0=0.0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
	aeqdSR.ImportFromProj4('+proj=aeqd  +lat_0=%0.12f +lon_0=%0.12f +units=m'%(obsPt[1], obsPt[0] ))

	# create transformations between the two SRs
	transform = osr.CoordinateTransformation(geoSr, aeqdSR)
	transformBack = osr.CoordinateTransformation(aeqdSR, geoSr)
	geo = gdal.Open(elevFile)
	(upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = geo.GetGeoTransform()

	geoT = {"upper_left_x":upper_left_x, "x_size":x_size, "upper_left_y":upper_left_y, "y_size":y_size, "x_pix": geo.RasterXSize, "y_pix":geo.RasterYSize}
	topography = geo.ReadAsArray()

	b = bbox(topography, 0, geo.RasterYSize-1, 0, geo.RasterXSize-1, geoT, transform, transformBack, maxDepth=7)
	v = np.array([-1,0,-0.5])
	v = v / np.linalg.norm(v)
	r = ray([0,0,2155], v)

	i = b.findIntersections(r)


