import vtk
from osgeo import gdal
from osgeo import osr, ogr
import numpy as np
from pycaster import pycaster
onid=1
import math, sys
import os

def getRotationMatrix(y, p, r):
	#print y
	cosa = math.cos(math.radians(y))
	sina = math.sin(math.radians(y))

	cosb = math.cos(math.radians(p))
	sinb = math.sin(math.radians(p))

	cosy = math.cos(math.radians(r))
	siny = math.sin(math.radians(r))

	r = np.matrix([[cosa*cosb, cosa*sinb*siny-sina*cosy, cosa*sinb*cosy+sina*siny],
				  [sina*cosb, sina*sinb*siny+cosa*cosy, sina*sinb*cosy-cosa*siny],
				  [-1.0*sinb, cosb*siny, cosb*cosy]])
	return r


class View():
	def __init__(self, **kwargs):
		self.obsLat = kwargs.get("obsLat", 0.0)
		self.obsLon = kwargs.get("obsLon", 0.0)
		self.obsElev = kwargs.get("obsElev", 0.0)
		self.horizAngle = kwargs.get("horizAngle", 0.0)
		self.vertAngle = kwargs.get("vertAngle", 0.0)
		self.heading = kwargs.get("heading", 0.0)
		self.elevationAngle = kwargs.get("elevationAngle", 0.0)
		self.angle1Start = self.heading-0.5*self.horizAngle#kwargs.get("angle1Start", 0.0)
		self.angle2Start = self.elevationAngle-0.5*self.vertAngle#kwargs.get("angle2Start", 0.0)
		self.angle1End = self.heading+0.5*self.horizAngle#kwargs.get("angle1End", 0.0)
		self.angle2End = self.elevationAngle+0.5*self.vertAngle#kwargs.get("angle2End", 0.0)
		self.maxRange = kwargs.get("maxRange", 1000)
		self.terrainFile = kwargs.get("terrainFile", "")

		self.geoSr = osr.SpatialReference()
		self.geoSr.ImportFromEPSG(4326)
		self.lccSR = osr.SpatialReference()
		#self.lccSR.ImportFromProj4('+proj=lcc +lon_0=-97 +lat_0=40 +lat_1=33 +lat_2=45 +x_0=0.0 +y_0=0.0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
		self.lccSR.ImportFromProj4('+proj=aeqd  +lat_0=%0.12f +lon_0=%0.12f +units=m'%(self.obsLat, self.obsLon))

		self.transform = osr.CoordinateTransformation(self.geoSr, self.lccSR)
		self.transformBack = osr.CoordinateTransformation(self.lccSR, self.geoSr)


		self.obsPoint = ogr.Geometry(ogr.wkbPoint)
		self.obsPoint.AddPoint( self.obsLon, self.obsLat, self.obsElev	)
		self.obsPoint.Transform(self.transform)
		self.obsPointTuple = [self.obsPoint.GetX(), self.obsPoint.GetY(), self.obsPoint.GetZ()]
		self.obsPointMatrix = np.matrix([[self.obsPointTuple[0]],[self.obsPointTuple[1]],[self.obsPointTuple[2]]])

		self.eye = self.obsPointMatrix
		self.up = np.matrix([[0],[0],[1]])
		self.up = getRotationMatrix(0.0,0.0,self.elevationAngle)*self.up#tip the upvector back
		self.up = getRotationMatrix(-1.0*self.heading,0,0)*self.up#rotate the up vector the the correct heading

		self.viewVector = np.matrix([[0],[1],[0]])
		self.viewVector = getRotationMatrix(-1.0*self.heading, 0.0, self.elevationAngle)*self.viewVector
		self.n = self.viewVector*-1 #Normal vector of viewing plane
		up2 = [self.up[0][0].item(),self.up[1][0].item(), self.up[2][0].item()]
		n2 = [self.n[0][0].item(), self.n[1][0].item(), self.n[2][0].item()]
		self.u = np.cross(up2,n2)

		self.u = np.matrix([[self.u[0]], [self.u[1]], [self.u[2]]])
		self.u = self.u / np.linalg.norm(self.u)

		self.v = np.cross([self.n[0][0].item(), self.n[1][0].item(), self.n[2][0].item()],[self.u[0][0].item(), self.u[1][0].item(), self.u[2][0].item()])
		self.v = np.matrix([[self.v[0]], [self.v[1]], [self.v[2]]])
		self.H = 100
		self.W = self.H*1.33
		self.d = float(self.H) / (2*math.tan(math.radians(self.vertAngle / 2.0)))
		self.C = self.eye - self.n * self.d #view plane center
		self.L = self.C - self.u *self.W/2.0 - self.v*self.H/2.0


		#self.polyData = self.loadPolyData(self.terrainFile)
		#import pdb; pdb.set_trace()
		#self.caster = pycaster.rayCaster(self.polyData)
		self.caster = pycaster.rayCaster.fromSTL(self.terrainFile)
		print "File Loaded"

	def loadPolyData(self, terrainFile):
		reader = None
		if terrainFile.count('.stl') > 0:
			reader = vtk.vtkPLYReader()
		elif terrainFile.count('.ply')>0:
			reader = vtk.vtkSTLReader()

		reader.SetFileName(terrainFile)
		reader.Update()
		mapper = vtk.vtkPolyDataMapper()
		return reader.GetOutput()

	def divideGrid(self, g):
		"""
			Divides matrix g of shape (r, c) into a new grid of shape (r*2-1, c*2-1)
			Cell (i,j) in g becomes cell (i*2, j*2) in the divided grid.
			cells above the skyline will have the value nan, including newly introduced rows
		"""
		assert (g.shape[0] %2 == 1)
		assert (g.shape[1] % 2 == 1)
		#new grid shape is computed to insert new rows/cols between those of g.  Grid edges and central vector are presevered
		newShape = (g.shape[0] * 2 -1, g.shape[1] * 2 -1)
		cols = newShape[0]#int(math.ceil((self.angle1End-self.angle1Start)/deltaAngle1))+1
		rows = newShape[1]#int(math.ceil((self.angle2End-self.angle2Start)/deltaAngle2))+1
		ranges = np.zeros((rows, cols))

		oldRows = g.shape[0]
		oldCols = g.shape[1]

		for i in range(oldRows):
			for j in range(oldCols):
				if not np.isnan(g[i][j]):
					ranges[i*2][j*2] = g[i][j]
				else:
					#if the old cells below, over and diagonally below and over are all nan, then this cell and the adjacent cells can be nan
					if (j+1 < oldCols and i+1 < oldRows):
						if np.isnan(g[i][j+1]) and np.isnan(g[i+1][j]) and np.isnan(g[i+1][j+1]):
							ranges[i*2][j*2+1] = np.nan
							ranges[i*2+1][j*2] = np.nan
							ranges[i*2][j*2] = np.nan
							ranges[i*2+1][j*2+1] = np.nan
		return ranges



	def doSkyline(self, ranges):
		#prepare skyline export
		driver = ogr.GetDriverByName('ESRI Shapefile')
		shapeData = None
		if os.path.exists('skyline.shp'):
			files = os.listdir('./')
			for f in files:
				if f.startswith('skyline.'):
					os.remove(f)


		shapeData = driver.CreateDataSource('skyline.shp')
		layer = shapeData.CreateLayer('Skyline', self.geoSr, ogr.wkbMultiPoint)
		layerDef = layer.GetLayerDefn()
		skyline = ogr.Geometry(ogr.wkbMultiPoint)


		print "Seeking skyline"
		cols = ranges.shape[1]
		rows = ranges.shape[0]
		
		wx = self.W / float(cols)
		hy = self.H / float(rows)
		pSource = self.obsPointTuple

		#starting at the left top, move down to find the row where the skyline starts
		skyEnd = [0,0]		
		j = 0 #left column
		for i in range(rows-1, -1, -1): #move top to bottom
			if ranges[i][j] == 0: # need to compute the range
				planeVector = self.L + self.u*(j+0.5)*wx + self.v*(i+0.5)*hy

				d2 = planeVector-self.eye #obsRotMatrix*viewVector + p0m
				d2 = d2 / np.linalg.norm(d2)
				d2 = d2*self.maxRange
				d2 = self.eye+d2
				pTarget = [d2[0][0].item(), d2[1][0].item(), d2[2][0].item()]
				pts = self.caster.castRay(pSource, pTarget)
				if len(pts) != 0: 
					#print pts[0]
					pt = ogr.Geometry(ogr.wkbPoint)
					pt.AddPoint(pts[0][0], pts[0][1])
					pt.Transform(self.transformBack)
					skyline.AddGeometry(pt)
					dist = vtk.vtkMath.Distance2BetweenPoints(pSource, pts[0]) ** 0.5
					ranges[i][j] = dist
					prange = dist+1000
				else: #no intersection, set to nan
					ranges[i][j] = np.nan
			if not np.isnan(ranges[i][j]): #skyline located
				skyEnd = [i,j]
				print "Skyline located at %d %d  range %0.2f"%(skyEnd[0], skyEnd[1], ranges[i][j].item())
				break

		#next trace across
		i = skyEnd[0]
		j = skyEnd[1]+1
		seekGround = True
		while j < ranges.shape[1]: #move across
			sys.stdout.write("\r%0.8f"%((j)/float(cols-1)))
			sys.stdout.flush()
			if i > ranges.shape[0]: #never found the sky
				print "Never found the sky"
				seekGround=True
				i=ranges.shape[0]-1
				j+=1
				continue
			if i< 0: #never found the ground
				print "Never found ground"
				seekGround = True
				i=0
				j+=1
			if ranges[i][j] == 0: # need to compute the range
				planeVector = self.L + self.u*(j+0.5)*wx + self.v*(i+0.5)*hy

				d2 = planeVector-self.eye #obsRotMatrix*viewVector + p0m
				d2 = d2 / np.linalg.norm(d2)
				d2 = d2*self.maxRange
				d2 = self.eye+d2
				pTarget = [d2[0][0].item(), d2[1][0].item(), d2[2][0].item()]

				#print pTarget[2]
				pts = self.caster.castRay(pSource, pTarget)
				if len(pts) != 0: 
					#print pts[0]
					pt = ogr.Geometry(ogr.wkbPoint)
					pt.AddPoint(pts[0][0], pts[0][1])
					pt.Transform(self.transformBack)
					skyline.AddGeometry(pt)
					dist = vtk.vtkMath.Distance2BetweenPoints(pSource, pts[0]) ** 0.5
					ranges[i][j] = dist
					prange = dist+1000
				else: #no intersection, set to nan
					ranges[i][j] = np.nan
			if np.isnan(ranges[i][j]): #this is sky
				if seekGround: #move down and try again
					i-=1 #move down
					continue #keep looking for ground
				else: #we are looking for sky and found it
					seekGround = True # switch to sky searc
					i -= 1 #move down to the ground again???
					continue
			else: #this is ground
				if seekGround: #found the ground we were looking for, all above are sky
					ranges[i+1:rows, j] = np.nan
					j += 1 # move over
					seekGround = False
					continue
				else: # looking for sky, move up
					i += 1
					continue
		
		#Finish skyline point export
		featureIndex=0
		feature = ogr.Feature(layerDef)
		feature.SetGeometry(skyline)
		feature.SetFID(featureIndex)
		layer.CreateFeature(feature)
		shapeData.Destroy()
		return ranges




	def computeRanges(self, prevGrid=None, divideGrid=False):
		"""
			p0 - Observation point
			d - scalar distance for max ray length
			arr - DEM

		"""
		#Changes to implement:
		#	Don't use delta angle, instead use the code here to compute view vectors: http://www.unknownroad.com/rtfm/graphics/rt_eyerays.html
		#	1. Compute tan(fovx) and tan(fovy)
		#	2. Iterate pixels (u,v)
		#		a. Compute vectors for u/v using tangents
		#		b. Rotate computed vectors per camera heading, tilt, (roll?)
		#		c. Translate vectors to camera position
		#	3. Separate skyline finding, this may not be necessary when seeded with a lower res matrix
		#	4. Implement range reduction to save on intersection computation
		#		Begin with max range until skyline is found
		#		Trace downward using range of pixel above plus some added buffer range, hopefully yields only one intersection

		#Start with the Eye and coordinate of interest
		if divideGrid:
			ranges = self.divideGrid(prevGrid)
		else:
			ranges = prevGrid
		
		rows = ranges.shape[0]
		cols = ranges.shape[1]

		
		wx = self.W / float(cols)
		hy = self.H / float(rows)

		pSource = self.obsPointTuple
		p0m = self.obsPointMatrix
		#new grid shape is computed to insert new rows/cols between those of prevGrid.  Grid edges and central vector are presevered
		ranges = self.doSkyline(ranges)

		prange = self.maxRange
		#matrix 0,0 is bottom left

		for j in range(cols): #left to right
			prange = self.maxRange
			sys.stdout.write("\r%0.8f"%((j)/float(cols-1)))
			sys.stdout.flush()

			for i in range(rows-1,-1,-1):#top to bottom
				if ranges[i][j] == 0 and not np.isnan(ranges[i][j]):
					
					planeVector = self.L + self.u*(j+0.5)*wx + self.v*(i+0.5)*hy

					d2 = planeVector-self.eye #obsRotMatrix*viewVector + p0m
					d2 = d2 / np.linalg.norm(d2)
					d2 = d2*prange
					d2 = self.eye+d2
					pTarget = [d2[0][0].item(), d2[1][0].item(), d2[2][0].item()]
					#print pTarget[2]
					pts = self.caster.castRay(pSource, pTarget)
					if len(pts) != 0: 
						#print pts[0]
						dist = vtk.vtkMath.Distance2BetweenPoints(pSource, pts[0]) ** 0.5
						ranges[i][j] = dist
						prange = dist+100
					# else:
					# 	break, this relies on bottom up sweep, doing top down this won't work

			if (j % 100 == 0): 
				print "Array dump"
				np.save("arrayDump", ranges)

		ranges[ ranges <= 0.0 ] = np.nan
		np.save("arrayDump", ranges)
		sys.stdout.write("\n\n")
		sys.stdout.flush()
		return ranges



	
if __name__ == "__main__":
	v = View(obsLat=40.596496, obsLon=-105.165740, obsElev=1697, horizAngle=73, vertAngle=54.75, heading=292.19, elevationAngle=-4.8, terrainFile="Test6.stl", maxRange=12000)
	# v = View(obsLat=40.587316, obsLon=-105.152516, obsElev=1597, horizAngle=73, vertAngle=54.75, heading=304, elevationAngle=4.2, terrainFile="Test5.stl", maxRange=12000)
	onid =40
	#prev = np.load("arrayDump2.npy")
	prev = np.zeros((33,33))
	#prev = v.doSkyline(prev)
	#np.save("skydump.npy", prev)
	for i in range(10):
		prev = v.computeRanges(prevGrid=prev)
		onid += 1

