import vtk
import random
import numpy
import math
from osgeo import gdal
from osgeo import osr, ogr
import math, sys
import numpy as np


# User provides an observaion point, heading, range, horizontal viewing angle, path to elevation file
# using observation point, range and heading and viewing angle compute 3 points
#   central view point
#   left most view point
#   right most view point
# extraction area is the rectangle which captures all these points
# perform extraction with range specific height reduction for earth curvature







def idxToPoint(i, j, l, t, x_size, y_size):#convert an grid index to a point
    x = l+i*x_size
    y = t+j*y_size
    return (x+0.5*x_size,y+0.5*y_size)

def getRotationMatrix(y, p, r):# get a rotation matrix with given yaw(z), pitch(y), roll(x)
    #print y
    cosa = math.cos(math.radians(y))
    sina = math.sin(math.radians(y))

    cosb = math.cos(math.radians(p))
    sinb = math.sin(math.radians(p))

    cosy = math.cos(math.radians(r))
    siny = math.sin(math.radians(r))

    r = np.matrix([[cosa*cosb, cosa*sinb*siny-sina*cosy, cosa*sinb*cosy+sina*siny],
                  [sina*cosb, sina*sinb*siny+cosa*cosy, sina*sinb*cosy-cosa*siny],
                  [-1.0*sinb, cosb*siny, cosb*cosy]])
    return r

def convertCoords(x,y,z, t):#convert some x,y,z coordiates with transform t
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(x,y)
    point.Transform(t)
    return (point.GetX(), point.GetY(), z)

def adjustForCurvature(obsPt, otherPt):
    #get the range between the points
    dist = ((obsPt[0]-otherPt[0])**2 + (obsPt[1]-otherPt[1])**2)**0.5
    r = 6371000+obsPt[2]
    off = r - (r**2 - dist**2)**0.5
    newZ = otherPt[2] - off
    return newZ


def getPolyData(obsPt, heading, viewingAngle, maxRange, elevFile, outputPly):
    # Arrange the input/output spatial references, points will be given in WGS84 (geoSr), vectors will be computed in LCC (lccSR)
    geoSr = osr.SpatialReference()
    geoSr.ImportFromEPSG(4326)
    aeqdSR = osr.SpatialReference()
    #lccSR.ImportFromProj4('+proj=lcc +lon_0=-97 +lat_0=40 +lat_1=33 +lat_2=45 +x_0=0.0 +y_0=0.0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
    aeqdSR.ImportFromProj4('+proj=aeqd  +lat_0=%0.12f +lon_0=%0.12f +units=m'%(obsPt[1], obsPt[0] ))

    # create transformations between the two SRs
    transform = osr.CoordinateTransformation(geoSr, aeqdSR)
    transformBack = osr.CoordinateTransformation(aeqdSR, geoSr)

    #create a unit vector for view projections
    viewVector = np.matrix([[1.0],[0],[0.0]])
    #compute the projected observation point
    obsPtaeqd = convertCoords(obsPt[0], obsPt[1], obsPt[2], transform)
    obsPtMatrix = np.matrix([[obsPtaeqd[0]],[obsPtaeqd[1]],[obsPtaeqd[2]]])
    obsPtMatrix = obsPtMatrix - getRotationMatrix(-1.0*heading+90, 0,0,)*viewVector*1000 #buffer the obs point backwards by 1km
    obsPtBuff = [obsPtMatrix[0][0].item(), obsPtMatrix[1][0].item(), obsPtMatrix[2][0].item()]
    obsPtBuffGeo = convertCoords(obsPtBuff[0], obsPtBuff[1], obsPtBuff[2], transformBack)

    centralPoint = obsPtMatrix + getRotationMatrix(-1.0*heading+90, 0,0,)*viewVector*maxRange
    centralPoint = [centralPoint[0][0].item(), centralPoint[1][0].item(), centralPoint[2][0].item()]
    centralPointGeo = convertCoords(centralPoint[0], centralPoint[1], centralPoint[2], transformBack)

    a1 = heading - 0.5*viewingAngle
    a2 = heading + 0.5*viewingAngle

    leftPoint = obsPtMatrix + getRotationMatrix(-1.0*a1+90, 0,0)*viewVector*maxRange
    leftPoint = [leftPoint[0][0].item(), leftPoint[1][0].item(), leftPoint[2][0].item()]
    leftPointGeo = convertCoords(leftPoint[0], leftPoint[1], leftPoint[2], transformBack)

    rightPoint = obsPtMatrix + getRotationMatrix(-1.0*a2+90, 0,0)*viewVector*maxRange
    rightPoint = [rightPoint[0][0].item(), rightPoint[1][0].item(), rightPoint[2][0].item()]
    rightPointGeo = convertCoords(rightPoint[0], rightPoint[1], rightPoint[2], transformBack)



    P1 = (min(centralPointGeo[0], leftPointGeo[0], rightPointGeo[0], obsPtBuffGeo[0]), max(centralPointGeo[1], leftPointGeo[1], rightPointGeo[1], obsPtBuffGeo[1]))
    P2 = (max(centralPointGeo[0], leftPointGeo[0], rightPointGeo[0], obsPtBuffGeo[0]), min(centralPointGeo[1], leftPointGeo[1], rightPointGeo[1], obsPtBuffGeo[1]))

    print P1
    print P2

    geo = gdal.Open(elevFile)
    (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = geo.GetGeoTransform()

    #import pdb; pdb.set_trace()
    upper_right_x = upper_left_x + x_size*geo.RasterXSize
    upper_right_y = upper_left_y
    lower_left_x = upper_left_x
    lower_left_y = upper_left_y + y_size*geo.RasterYSize
    lower_right_x = upper_right_x
    lower_right_y = upper_left_y + y_size*geo.RasterYSize

    if (P1[0] < upper_left_x or P1[0] > upper_right_x or P1[1] > upper_left_y or P1[1] < lower_left_y):
        raise Exception("Point not in terrain file %0.8f %0.8f"%tuple(P1))
    if (P2[0] < upper_left_x or P2[0] > upper_right_x or P2[1] > upper_left_y or P2[1] < lower_left_y):
        raise Exception("Point not in terrain file %0.8f %0.8f"%tuple(P2))







    point = ogr.Geometry(ogr.wkbPoint)

    pc1 = [int(math.floor((P1[0] - upper_left_x)/x_size)), int(math.floor( ( P1[1] - upper_left_y) / y_size ))]
    pc2 = [int(math.floor((P2[0] - upper_left_x)/x_size)), int(math.floor( ( P2[1] - upper_left_y) / y_size ))]

    print pc1
    print pc2 

    # Make a 32 x 32 grid
    xsize = pc2[0] - pc1[0]
    ysize = pc2[1] - pc1[1]
     
    print ("%d %d " %(xsize, ysize))


    xoffset=pc1[0]
    xspan = xsize
    xcell = 10
    yoffset = pc1[1]
    ycell = 10
    yspan = ysize

    x0 = xoffset*xcell
    y0 = yoffset*ycell

    topography = geo.ReadAsArray(xoffset, yoffset, xspan, yspan)
     
    # Define points, triangles and colors
    points = vtk.vtkPoints()
     
    # Build the meshgrid manually
    count = 0
    minz = None
    maxz = None 


    pointSource = vtk.vtkProgrammableSource()

    def readPoints():
        output = pointSource.GetPolyDataOutput()
        points = vtk.vtkPoints()
        output.SetPoints(points)

        for i in range(xsize-1, 1, -1):
            sys.stdout.write("\r%0.8f"%((xsize-i)/float(xsize-1)))
            sys.stdout.flush()
            for j in range(ysize-1):
                cx = xoffset+i
                cy = yoffset+j
                z1 = topography[j][i]
                
                c1x, c1y = idxToPoint(cx, cy, upper_left_x, upper_left_y, x_size, y_size)
                p1x, p1y, p1z = convertCoords(c1x, c1y, z1, transform)
                points.InsertNextPoint(p1x, p1y, p1z)

    pointSource.SetExecuteMethod(readPoints)

    # Construct the surface and create isosurface.
    surf = vtk.vtkSurfaceReconstructionFilter()
    surf.SetInputConnection(pointSource.GetOutputPort())

    cf = vtk.vtkContourFilter()
    cf.SetInputConnection(surf.GetOutputPort())
    cf.SetValue(0, 0.0)
    

    # Sometimes the contouring algorithm can create a volume whose gradient
    # vector and ordering of polygon (using the right hand rule) are
    # inconsistent. vtkReverseSense cures this problem.
    # reverse = vtk.vtkReverseSense()
    # reverse.SetInputConnection(cf.GetOutputPort())
    # reverse.ReverseCellsOn()
    # reverse.ReverseNormalsOn()


    plyWriter = vtk.vtkPLYWriter()
    plyWriter.SetFileName(outputPly)
    plyWriter.SetInputConnection(cf.GetOutputPort())
    plyWriter.Write()


    return cf

def renderPolyData(plyFile, elev, heading, pitch, viewAngle):

    

    # Read and display for verification
    reader = vtk.vtkPLYReader()
    reader.SetFileName(plyFile)

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(reader.GetOutput())
    else:
        mapper.SetInputConnection(reader.GetOutputPort())
     
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    # Create a rendering window and renderer
    ren = vtk.vtkRenderer()
    cam = vtk.vtkCamera()
    
    ren.SetActiveCamera(cam)

    cam.SetPosition(0,0,elev)
    cam.SetViewUp(0,0,1)
    cam.SetFocalPoint(0,1000, elev)
    cam.Yaw(-1.0*heading)
    cam.Pitch(pitch)
    cam.SetViewAngle(viewAngle)
    cam.SetClippingRange(0.1,18000)

    lightKit = vtk.vtkLightKit()
    lightKit.AddLightsToRenderer(ren)

    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
     
    # Create a renderwindowinteractor
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)
     
    # Assign actor to the renderer
    ren.AddActor(actor)
     
    # Enable user interface interactor
    iren.Initialize()
    renWin.Render()
    iren.Start()

if __name__ == "__main__":
    #obsPt = (-105.152516, 40.587316,  1597) # WGS84 Lon Lat
    obsPt = (-105.165751, 40.596423,  1697) # WGS84 Lon Lat
    heading = 292.19 #compass direction
    viewingAngle = 100 #degrees
    maxRange = 12000 #range in meters
    elevFile = '/home/dustin/elev/usgs_ned_13_n41w106_gridfloat.flt'
    outputStl = 'Test7.stl'

    getPolyData(obsPt, heading, viewingAngle, maxRange, elevFile, outputStl)
    #renderPolyData(outputStl)