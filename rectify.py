import numpy as np 
import scipy.misc as sm
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg 
import math
from scipy.linalg import lu, inv
import sys
import json 
import cv2
import os
def gausselim(A,B):
	"""
	Solve Ax = B using Gaussian elimination and LU decomposition.
	A = LU   decompose A into lower and upper triangular matrices
	LUx = B  substitute into original equation for A
	Let y = Ux and solve:
	Ly = B --> y = (L^-1)B  solve for y using "forward" substitution
	Ux = y --> x = (U^-1)y  solve for x using "backward" substitution
	:param A: coefficients in Ax = B
	:type A: numpy.ndarray of size (m, n)
	:param B: dependent variable in Ax = B
	:type B: numpy.ndarray of size (m, 1)
	"""
	# LU decomposition with pivot
	pl, u = lu(A, permute_l=True)
	# forward substitution to solve for Ly = B
	y = np.zeros(B.size)
	for m, b in enumerate(B.flatten()):
		y[m] = b
		# skip for loop if m == 0
		if m:
			for n in xrange(m):
				y[m] -= y[n] * pl[m,n]
		y[m] /= pl[m, m]

	# backward substitution to solve for y = Ux
	x = np.zeros(B.size)
	lastidx = B.size - 1  # last index
	for midx in xrange(B.size):
		m = B.size - 1 - midx  # backwards index
		x[m] = y[m]
		if midx:
			for nidx in xrange(midx):
				n = B.size - 1  - nidx
				x[m] -= x[n] * u[m,n]
		x[m] /= u[m, m]
	return x

def GENP(A, b):
	'''
	Gaussian elimination with no pivoting.
	% input: A is an n x n nonsingular matrix
	%		b is an n x 1 vector
	% output: x is the solution of Ax=b.
	% post-condition: A and b have been modified. 
	'''
	n =  len(A)
	if b.size != n:
		raise ValueError("Invalid argument: incompatible sizes between A & b.", b.size, n)
	for pivot_row in xrange(n-1):
		for row in xrange(pivot_row+1, n):
			multiplier = A[row][pivot_row]/A[pivot_row][pivot_row]
			#the only one in this column since the rest are zero
			A[row][pivot_row] = multiplier
			for col in xrange(pivot_row + 1, n):
				A[row][col] = A[row][col] - multiplier*A[pivot_row][col]
			#Equation solution column
			b[row] = b[row] - multiplier*b[pivot_row]
	print A
	print b
	x = np.zeros(n)
	k = n-1
	x[k] = b[k]/A[k,k]
	while k >= 0:
		x[k] = (b[k] - np.dot(A[k,k+1:],x[k+1:]))/A[k,k]
		k = k-1
	return x

def GEPP(A, b):
	'''
	Gaussian elimination with partial pivoting.
	% input: A is an n x n nonsingular matrix
	%		b is an n x 1 vector
	% output: x is the solution of Ax=b.
	% post-condition: A and b have been modified. 
	'''
	n =  len(A)
	if b.size != n:
		raise ValueError("Invalid argument: incompatible sizes between A & b.", b.size, n)
	# k represents the current pivot row. Since GE traverses the matrix in the upper 
	# right triangle, we also use k for indicating the k-th diagonal column index.
	for k in xrange(n-1):
		#Choose largest pivot element below (and including) k
		maxindex = abs(A[k:,k]).argmax() + k
		if A[maxindex, k] == 0:
			raise ValueError("Matrix is singular.")
		#Swap rows
		if maxindex != k:
			A[[k,maxindex]] = A[[maxindex, k]]
			b[[k,maxindex]] = b[[maxindex, k]]
		for row in xrange(k+1, n):
			multiplier = A[row][k]/A[k][k]
			#the only one in this column since the rest are zero
			A[row][k] = multiplier
			for col in xrange(k + 1, n):
				A[row][col] = A[row][col] - multiplier*A[k][col]
			#Equation solution column
			b[row] = b[row] - multiplier*b[k]
	print A
	print b
	x = np.zeros(n)
	k = n-1
	x[k] = b[k]/A[k,k]
	while k >= 0:
		x[k] = (b[k] - np.dot(A[k,k+1:],x[k+1:]))/A[k,k]
		k = k-1
	return x


def showImages(rangeMatrixFileName, imageFileName):
	im = mpimg.imread(imageFileName)
	ranges = np.load(rangeMatrixFileName)
	ranges[np.isnan(ranges)] = 0
	ranges = np.flipud(ranges)
	#ranges = sm.imresize(ranges, (im.shape[0], im.shape[1]))
	fig, axes = plt.subplots(nrows=2, ncols=1)
	axes[0].imshow(ranges)
	axes[1].imshow(im)
	plt.show()



class clicker_class(object):
	def __init__(self, ax1, ax2, ax3, outputPointsFile, pix_err=1):
		self.canvas = ax1.get_figure().canvas
		self.ax1 = ax1
		self.ax2 = ax2
		self.ax3 = ax3
		self.cid = None
		self.pointsFile = outputPointsFile
		self.pt1_lst = []
		self.pt1_lbl = []
		self.pt2_lst = []
		self.pt2_lbl = []
		self.pt3_lst = []
		self.pt3_lbl = []
		self.pt1_plot = ax1.plot([], [], marker='o', c="lightgreen",
							   linestyle='none', zorder=5)[0]

		self.pt2_plot = ax2.plot([], [], marker='o', c="lightgreen",
							   linestyle='none', zorder=5)[0]
		self.pt3_plot = ax3.plot([], [], marker='o', c="lightgreen",
							   linestyle='none', zorder=5)[0]
		self.pix_err = pix_err
		self.connect_sf()

	def set_visible(self, visible):
		'''sets if the curves are visible '''
		self.pt1_plot.set_visible(visible)
		self.pt2_plot.set_visible(visible)
		self.pt3_plot.set_visible(visible)

	def clear(self, ax):
		'''Clears the points'''
		if ax == 1:
			self.pt1_lst = []
			for t in self.pt1_lbl:
				t.remove()
			self.pt1_lbl = []
			self.redraw(1)
		elif ax == 2:
			self.pt2_lst = []
			for t in self.pt2_lbl:
				t.remove()
			self.pt2_lbl = []
			self.redraw(2)
		elif ax == 3:
			self.pt3_lst = []
			for t in self.pt3_lbl:
				t.remove()
			self.pt3_lbl = []
			self.redraw(3)

	def connect_sf(self):
		if self.cid is None:
			self.cid = self.canvas.mpl_connect('button_press_event',
											   self.click_event)

	def disconnect_sf(self):
		if self.cid is not None:
			self.canvas.mpl_disconnect(self.cid)
			self.cid = None

	def loadPoints(self, filename):
		if not os.path.exists(filename):
			return
		f = open(filename, 'r')
		c = json.load(f)
		i=0
		for pt in c["RangePoints"]:
			self.pt1_lst.append(pt)
			self.pt1_lbl.append(self.ax1.text(pt[0],pt[1],i+1, verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
			self.pt3_lst.append(pt)
			self.pt3_lbl.append(self.ax3.text(pt[0],pt[1],i+1, verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
			i +=1
		i=0
		for pt in c["ImagePoints"]:
			self.pt2_lst.append(pt)
			self.pt2_lbl.append(self.ax2.text(pt[0],pt[1],i+1, verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
			
			i +=1
		self.redraw(1)
		self.redraw(2)
		self.redraw(3)

	def click_event(self, event):
		if self.ax1 == event.inaxes or self.ax3 == event.inaxes: 
			''' Extracts locations from the user'''
			if event.key == 'control':
				self.clear(1)
				self.clear(3)
				return
			
			if event.xdata is None or event.ydata is None:
				return
			if event.button == 1 and event.key == 'shift':
				self.pt1_lst.append((event.xdata, event.ydata))
				self.pt1_lbl.append(self.ax1.text(event.xdata,event.ydata,str(len(self.pt1_lst)), verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
				self.pt3_lst.append((event.xdata, event.ydata))
				self.pt3_lbl.append(self.ax3.text(event.xdata,event.ydata,len(self.pt3_lst), verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
				print ((event.xdata, event.ydata))
			elif event.button == 3:
				self.remove_pt((event.xdata, event.ydata), 1)
				self.remove_pt((event.xdata, event.ydata), 3)
			self.redraw(1)
			self.redraw(3)

		if self.ax2 == event.inaxes:
			''' Extracts locations from the user'''
			if event.key == 'control':
				self.clear(2)
				return
			if event.xdata is None or event.ydata is None:
				return
			if event.button == 1 and event.key == 'shift':
				self.pt2_lst.append((event.xdata, event.ydata))
				self.pt2_lbl.append(self.ax2.text(event.xdata,event.ydata,len(self.pt2_lst), verticalalignment="bottom", horizontalalignment="center", bbox={"facecolor":"white", "alpha":0.4, "boxstyle":"Round"}))
				
				print ((event.xdata, event.ydata))
			elif event.button == 3:
				self.remove_pt((event.xdata, event.ydata), 2)
			self.redraw(2)

	def remove_pt(self, loc, ax):
		if ax == 1:
			if len(self.pt1_lst) > 0:
				idx = np.argmin(map(lambda x:
									  np.sqrt((x[0] - loc[0]) ** 2 +
											  (x[1] - loc[1]) ** 2),
									  self.pt1_lst))
				self.pt1_lst.pop(idx)
				lbl = self.pt1_lbl.pop(idx)
				lbl.remove()
				i = idx
				for l in self.pt1_lbl[idx:]:
					l.set_text(str(i+1))
					i+=1
		if ax == 2:
			if len(self.pt2_lst) > 0:
				idx = np.argmin(map(lambda x:
									  np.sqrt((x[0] - loc[0]) ** 2 +
											  (x[1] - loc[1]) ** 2),
									  self.pt2_lst))
				self.pt2_lst.pop(idx)
				lbl = self.pt2_lbl.pop(idx)
				lbl.remove()
				i = idx
				for l in self.pt2_lbl[idx:]:
					l.set_text(str(i+1))
					i+=1
		if ax == 3:
			if len(self.pt3_lst) > 0:
				idx = np.argmin(map(lambda x:
									  np.sqrt((x[0] - loc[0]) ** 2 +
											  (x[1] - loc[1]) ** 2),
									  self.pt3_lst))
				self.pt3_lst.pop(idx)
				lbl = self.pt3_lbl.pop(idx)
				lbl.remove()
				i = idx
				for l in self.pt3_lbl[idx:]:
					l.set_text(str(i+1))
					i+=1

	def redraw(self, ax):
		if ax == 1:
			if len(self.pt1_lst) > 0:
				x, y = zip(*self.pt1_lst)
			else:
				x, y = [], []
			self.pt1_plot.set_xdata(x)
			self.pt1_plot.set_ydata(y)
		elif ax == 2:
			if len(self.pt2_lst) > 0:
				x, y = zip(*self.pt2_lst)
			else:
				x, y = [], []
			self.pt2_plot.set_xdata(x)
			self.pt2_plot.set_ydata(y)
		elif ax == 3:	
			if len(self.pt3_lst) > 0:
				x, y = zip(*self.pt3_lst)
			else:
				x, y = [], []
			self.pt3_plot.set_xdata(x)
			self.pt3_plot.set_ydata(y)
		self.dumpPoints()
		self.canvas.draw()

	def return_points(self):
		'''Returns the clicked points in the format the rest of the
		code expects'''
		return np.vstack(self.pt1_lst).T

	def dumpPoints(self):
		f = open(self.pointsFile, "w")
		p = {"RangePoints":self.pt1_lst, "ImagePoints":self.pt2_lst}
		json.dump(p,f)
		f.close()


def showClicker(im, rng, elev, initialPointsFile=None, outputPointsFile=None):
	#ax = plt.gca()
	#implot = ax.imshow(im)
	ax1 = plt.subplot2grid((2,2), (0,0))#plt.subplot(2,1,1)
	ax3 = plt.subplot2grid((2,2), (0,1), sharex=ax1, sharey=ax1)
	ax2 = plt.subplot2grid((2,2), (1,0), colspan=2)#plt.subplot(2,1,2)#, sharex=ax1, sharey=ax1)
	ax1.imshow(rng, interpolation="nearest")
	ax3.imshow(elev,  interpolation="nearest")
	cc =clicker_class(ax1, ax2, ax3, outputPointsFile)

	if initialPointsFile != None:
		cc.loadPoints(initialPointsFile)

	ax2.imshow(im)

	plt.show()

#if __name__ == "__main__":



def rectify2(rangePath, imagePath, correspondencePath, outGridName=None, outImageName=None):
	#Pic Points x
	#correspondencePath = "_3correspondencePoints.json"
	#imageFileName =  'IMG_0981.png'
	#rangeMatrixFileName = 'arrayDump.npy'
	f = open(correspondencePath, 'r')
	c = json.load(f)
	srcPts = np.array(c["RangePoints"])
	dstPts = np.array(c["ImagePoints"])


	H = cv2.findHomography(srcPts, dstPts, method=cv2.LMEDS)
	Hinv = inv(H[0])
	im = mpimg.imread(imagePath)
	ranges = np.load(rangePath)
	ranges[np.isnan(ranges)] = 0
	ranges = np.flipud(ranges)

	#create a matrix to match the image
	newRange = np.zeros((im.shape[0], im.shape[1]))
	print("\n\n")
	#iterate the new matrix
	for i in range(newRange.shape[0]):
		sys.stdout.write("\r%3d%%"%(int(i/float(newRange.shape[0])*100)))
		sys.stdout.flush()
		for j in range(newRange.shape[1]):
			#for each cell apply the Hinv transform
			#scale the transformed points
			p0 = np.matrix([[j],[i],[1]])
			pt = Hinv*p0
			x = int(round(pt[0][0].item()/pt[2][0].item()))
			y = int(round(pt[1][0].item()/pt[2][0].item()))
			#confirm transformed point in shape of range matrix
			if (x < ranges.shape[1] and x >= 0 and y < ranges.shape[0] and y >= 0):
				#read value from range matrix
				newRange[i][j] = ranges[y][x]

	if outGridName is not None:
		np.save(outGridName,newRange)
	if outImageName is not None:
		mpimg.imsave(outImageName, newRange)


def rectify(rangePath, picPath):
	#Pic Points x
	imageFileName =  'IMG_0981.png'
	rangeMatrixFileName = 'arrayDump.npy'

	im = mpimg.imread(imageFileName)
	ranges = np.load(rangeMatrixFileName)
	ranges[np.isnan(ranges)] = 0
	ranges = np.flipud(ranges)

	u = [
			974.0,
			3344.0,
			3691.0,
			2505.0
		]

	#pic points y
	v = [
			992.0,
			1060.0,
			969.0,
			1364.0
		 ]

	#range points x
	x = [
			837.0,
			3145.0,
			3464.0,
			2355.0
		]

	#range points y
	y = [
			1044.0,
			1126.0,
			1044.0,
			1418.0
		]
	
	A = np.array([
			[x[0], y[0], 1, 0   , 0   , 0, -1*x[0]*u[0], -1*y[0]*u[0]],
			[x[1], y[1], 1, 0   , 0   , 0, -1*x[1]*u[1], -1*y[1]*u[1]],
			[x[2], y[2], 1, 0   , 0   , 0, -1*x[2]*u[2], -1*y[2]*u[2]],
			[x[3], y[3], 1, 0   , 0   , 0, -1*x[3]*u[3], -1*y[3]*u[3]],
			[0   , 0   , 0, x[0], y[0], 1, -1*x[0]*v[0], -1*y[0]*v[0]],
			[0   , 0   , 0, x[1], y[1], 1, -1*x[1]*v[1], -1*y[1]*v[1]],
			[0   , 0   , 0, x[2], y[2], 1, -1*x[2]*v[2], -1*y[2]*v[2]],
			[0   , 0   , 0, x[3], y[3], 1, -1*x[3]*v[3], -1*y[3]*v[3]]
		])

			
	B = np.array([
			[u[0]],
			[u[1]],
			[u[2]],
			[u[3]],
			[v[0]],
			[v[1]],
			[v[2]],
			[v[3]]
		])

	#x = gausselim(A,B)
	X = GEPP(A,B)
	H = np.array([[X[0],X[1],X[2]],[X[3],X[4],X[5]],[X[6],X[7],1]])
	Hinv = np.linalg.inv(H)

	#create a matrix to match the image
	newRange = np.zeros(ranges.shape)
	print("\n\n")
	#iterate the new matrix
	for i in range(newRange.shape[0]):
		sys.stdout.write("\r%3d%%"%(int(i/float(newRange.shape[0])*100)))
		sys.stdout.flush()
		for j in range(newRange.shape[1]):
			#for each cell apply the Hinv transform
			#scale the transformed points
			p0 = np.matrix([[j],[i],[1]])
			pt = Hinv*p0
			x = int(math.floor(pt[0][0].item() / pt[2][0].item()))
			y = int(math.floor(pt[1][0].item() / pt[2][0].item()))
			#confirm transformed point in shape of range matrix
			if (x < newRange.shape[1] and x > 0 and y < newRange.shape[0] and y > 0):
				#read value from range matrix
				newRange[i][j] = ranges[y][x]

	np.save("rectified.npy",newRange)
	fig, axes = plt.subplots(nrows=2, ncols=2)
	axes[0].imshow(ranges)
	axes[1].imshow(im)
	axes[2].imshow(newRange)
	plt.show()
    



