from osgeo import gdal
from osgeo import osr, ogr
import math
import numpy as np
import cPickle as pkl
onid = 14
def getRotationMatrix(y, p, r):
	#print y
	cosa = math.cos(math.radians(y))
	sina = math.sin(math.radians(y))

	cosb = math.cos(math.radians(p))
	sinb = math.sin(math.radians(p))

	cosy = math.cos(math.radians(r))
	siny = math.sin(math.radians(r))

	r = np.matrix([[cosa*cosb, cosa*sinb*siny-sina*cosy, cosa*sinb*cosy+sina*siny],
				  [sina*cosb, sina*sinb*siny+cosa*cosy, sina*sinb*cosy-cosa*siny],
				  [-1.0*sinb, cosb*siny, cosb*cosy]])
	return r
def getScaleMatrix(sx, sy, sz):
	return np.matrix([[sx,0,0],[0,sy,0],[0,0,sz]])

def pointToIdx(x, y, l, r, t, b, rows, cols):
	#print rows
	#print cols
	assert(l<=x)
	assert(x<=r)
	assert(y<=t)
	assert(b<=y)
	offx = x - l
	spanx = math.fabs(r-l)
	xs = spanx/float(cols)

	xcell = offx / xs
	xcell -= offx % xs

	xcell = int(math.floor(xcell))

	offy = t-y
	spany = math.fabs(t-b)
	ys = spany/float(rows)

	ycell = offy / ys
	ycell -= offy % ys

	ycell = int(math.floor(ycell))

	return (xcell, ycell)

def idxToPoint(i, j, l, t, x_size, y_size):
	x = l+i*x_size
	y = t+j*y_size
	return (x+0.5*x_size,y+0.5*y_size)

def intersect(p0, d, arr, maxiters, maxval, transform=None, startz=None):
	"""
		p0: Observation point (x: grid cell, y: grid cell, z:elevation)
		d: direction and increment vector
		arr: surface values
		transform:
		startz: 
	"""

	p1 = p0

	#add d iteratively untill we reach startz, this allows us to start an intersect search at a specified z value along the ray
	# if (startz != None):
	# 	p2 = p1
	# 	while (p2[2][0] < startz):
	# 		p2 = p2 + d
	# 	p1 = p2

	count = 0
	zvector = p1[2][0].item()
	zsurface = arr[int(p1[1][0].item())][int(p1[0][0].item())].item()
	zvectorLast = -1.0

	#compute maxiters to leave grid or exceed elevation
	badVal = False
	while (zvector > zsurface):
		
		p1Last = p1	
		p1 = p1+d
		count += 1
		if p1[1][0].item() < 0 or p1[1][0].item() >= arr.shape[0] or p1[0][0].item() < 0 or p1[0][0].item() >= arr.shape[1]:
			print "Off grid"
			p1 = p1Last
			badVal = True
			break
		if   p1[2][0].item() > maxval or count > maxiters:
			print "Max iterations or value exceeded"
			p1 = p1Last
			badVal = True
			break
		zvector = p1[2][0].item()
		# if zvector != zvectorLast:
		# 	print zvector
		# 	zvectorLast = zvector
		zsurface = arr[int(p1[1][0].item())][int(p1[0][0].item())].item()

	if badVal:
		zsurface = -1.0
	p2 = idxToPoint(int(math.floor(p1[0][0].item())), int(math.floor(p1[1][0].item())), upper_left_x, upper_left_y, x_size, y_size)
	if transform != None:
		point = ogr.Geometry(ogr.wkbPoint)
		point.AddPoint(p2[0], p2[1], zsurface)
		point.Transform(transform)
		return point
	return (p2[0], p2[1], zsurface)

def skyline(p0, d, arr, dAngle=1.0, angleStart=0, angleEnd=360):
	ring = ogr.Geometry(ogr.wkbLinearRing)

	#result = []
	i = angleStart
	maxVal = np.amax(arr)
	while (i < angleEnd):
		d2 = getRotationMatrix(i,0,10.0)*d
		p = intersect(p0, d2, arr, 10000, maxVal)
		ring.AddPoint(p[0], p[1])
		#result.append(p)
		i += dAngle
		print i
	#return result

	geom = ogr.Geometry(ogr.wkbPolygon)
	geom.AddGeometry(ring)
	return geom

def view(p0, d, arr, deltaAngle1=1.0, angle1Start=0, angle1End=360, deltaAngle2=1.0, angle2Start=-10, angle2End=10, transform=None, pm=None, transformBack=None):
	multipoint = ogr.Geometry(ogr.wkbMultiPoint)
	cols = int((angle1End-angle1Start)/deltaAngle1)
	rows = int((angle2End-angle2Start)/deltaAngle2)
	ranges = np.zeros((rows, cols))
	#result = []
	i = angle1Start
	maxVal = np.amax(arr)
	colOn = 0

	while (i < angle1End):
		j= angle2End
		rowOn=rows-1
		startz = None
		while(j>angle2Start):
			d2 = getRotationMatrix(i,j,0)*d
			p = intersect(p0, d2, arr, 10000, maxVal, transform=transform, startz=startz)

			if transform == None:
				point1 = ogr.Geometry(ogr.wkbPoint)
				point1.AddPoint(p[0], p[1])
				point1.Transform(transformBack)
				multipoint.AddGeometry(point1)
			else:
				rng = 0
				if (p.GetZ() > 0):
					#print "has z"
					startz = p.GetZ()
					m = np.matrix([[p.GetX()], [p.GetY()], [p.GetZ()]])
					m2 = pm-m
					rng = np.linalg.norm(m2)
					#print rng
				else:
					j=angle2Start
					startz = None
				ranges[rowOn][colOn] = rng
				p.Transform(transformBack)
				multipoint.AddGeometry(p)
			j -=deltaAngle2
			rowOn -= 1
		i += deltaAngle1
		colOn += 1
		print i
	np.save("arrayDump%d"%onid, ranges)
	return multipoint


def skylinetest(p0, d, arr):
	r = skyline(p0, d, arr)

	driver = ogr.GetDriverByName('ESRI Shapefile') # will select the driver foir our shp-file creation.
	shapeData = driver.CreateDataSource("export.shp") #so there we will store our data
	layer = shapeData.CreateLayer('customs', geoSr, ogr.wkbPolygon) #this will create a corresponding layer for our data with given spatial information.
	layer_defn = layer.GetLayerDefn() # gets parameters of the current shapefile
	featureIndex = 0 #this will be the first point in our dataset
	##now lets write this into our layer/shape file:
	feature = ogr.Feature(layer_defn)
	feature.SetGeometry(r)
	feature.SetFID(featureIndex)
	layer.CreateFeature(feature)
	shapeData.Destroy()

def gridTest(p0, d, arr, transform=None, pm=None, transformBack=None):
	import time
	t0 = time.time()
	direction = 270.0
	hangle = 75#58.715507085583
	vangle = 45.0#41.112090439167
	a1FromNorth = 90
	a1Central = 250
	a1start = (a1Central-a1FromNorth)-0.5*hangle
	a1end = (a1Central-a1FromNorth)+0.5*hangle
	a2Central = -15
	a2start = a2Central-0.5*vangle
	a2end = a2Central+0.5*vangle

	r = view(p0, d, arr, angle1Start=a1start, angle1End=a1end, deltaAngle1=1.0, angle2Start=a2start, angle2End=a2end, deltaAngle2=1.0, transform=transform, pm=pm, transformBack=transformBack)

	driver = ogr.GetDriverByName('ESRI Shapefile') # will select the driver foir our shp-file creation.
	shapeData = driver.CreateDataSource("export%d.shp"%onid) #so there we will store our data
	layer = shapeData.CreateLayer('customs', geoSr, ogr.wkbMultiPoint) #this will create a corresponding layer for our data with given spatial information.
	layer_defn = layer.GetLayerDefn() # gets parameters of the current shapefile
	featureIndex = 0 #this will be the first point in our dataset
	##now lets write this into our layer/shape file:
	feature = ogr.Feature(layer_defn)
	feature.SetGeometry(r)
	feature.SetFID(featureIndex)
	layer.CreateFeature(feature)
	shapeData.Destroy()
	t1 = time.time()
	print "T0 %0.8f"%t0
	print "T1 %0.8f"%t1
	print "Elapsed %0.8f"%(t1-t0)

geo = gdal.Open('D:\\Projects\\ImageRanging\\USGS_NED_1-3_arc-second_n41w106_1_x_1_degree_GridFloat_2015\\usgs_ned_13_n41w106_gridfloat.flt')
arr = geo.ReadAsArray()
geoSr = osr.SpatialReference()
geoSr.ImportFromWkt(geo.GetProjection())

(upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = geo.GetGeoTransform()


#first point
x = -105.140908333333#-105.170655  #-105.132260 #-105.090476  #-105.098146  #upper_left_x + 0.5*x_size -105.098146  40.494566
y = 40.530736111111  #40.599671# 40.557471#40.472424#40.494566 #upper_left_y + 0.5*y_size
#x = upper_left_x + 1.5*x_size
#y = upper_left_y + 1.5*y_size
p = pointToIdx(x, y, upper_left_x, upper_left_x + x_size* arr.shape[1], upper_left_y, upper_left_y + y_size*arr.shape[0], arr.shape[0], arr.shape[1])

xcell = p[0]
ycell = p[1]
z = arr[p[1]][p[0]].item()+2

pc = np.matrix([[xcell],[ycell], [z]])

print p
point = ogr.Geometry(ogr.wkbPoint)
point.AddPoint(x, y, arr[p[1]][p[0]].item())

lccSR = osr.SpatialReference()
lccSR.ImportFromProj4('+proj=lcc +lon_0=-97 +lat_0=40 +lat_1=33 +lat_2=45 +x_0=0.0 +y_0=0.0 datum=WGS84')

transform = osr.CoordinateTransformation(geoSr, lccSR)
transformBack = osr.CoordinateTransformation(lccSR, geoSr)
point.Transform(transform)

pm = np.matrix([[point.GetX()],[point.GetY()],[z]])

d = np.matrix([[0],[10.0],[0]])
dc = np.matrix([[0.5],[0],[0]])

gridTest(pc, dc, arr, transform, pm, transformBack)
# skylinetest(pc, dc, arr)




# #point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
# #point.SetPoint(0, x, y)
# ##OR
# #point = ogr.CreateGeometryFromWkt("POINT (1120351.57 741921.42)")

# print ("Starting Point lat/lon %0.8f, %0.8f, %0.8f"%(point.GetX(), point.GetY(), point.GetZ()))

# point.Transform(transform)
# print ("Starting Point meters %0.8f, %0.8f, %0.8f"%(point.GetX(), point.GetY(), point.GetZ()))

# heading = math.radians(150.0)
# newx = math.cos(heading) * 1000 + point.GetX()
# newy = math.sin(heading) * 1000 + point.GetY()

# #p = pointToIdx(newx, newy, upper_left_x, upper_left_x + x_size* arr.shape[1], upper_left_y, upper_left_y + y_size*arr.shape[0], arr.shape[0], arr.shape[1])
# point2 = ogr.Geometry(ogr.wkbPoint)
# point2.AddPoint(newx, newy)
# print ("Rotated point by 150 meters: %0.8f, %0.8f, %0.8f"%(point2.GetX(), point2.GetY(), point2.GetZ()))

# pm = np.matrix([[point.GetX()], [point.GetY()], [point.GetZ()]])
# d = np.matrix([[1000], [0], [0]])
# r = getRotationMatrix(150,-10,0)

# rp = pm + (r*d)
# print rp
# print ("Matrix Rotated Point: %0.8f, %0.8f, %0.8f"%(rp[0][0].item(), rp[1][0].item(), rp[2][0].item()))



# point2.Transform(transformBack)
# print ("Rotated point by 15 lat/lon %0.12f, %0.12f, %0.12f"%(point2.GetX(), point2.GetY(), point2.GetZ()))


# # >>> geo.GetGeoTransform()
# # (-105.00055555489, 9.2592592525846e-05, 0.0, 40.000555555495446, 0.0, -9.2592592
# # 525846e-05)
# # >>> srs = osgeo.ogr.SpatialReference()
# # Traceback (most recent call last):
# #   File "<stdin>", line 1, in <module>
# # NameError: name 'osgeo' is not defined
# # >>> import osgeo
# # >>> srs = osgeo.ogr.SpatialReference()
# # Traceback (most recent call last):
# #   File "<stdin>", line 1, in <module>
# # AttributeError: '_Module' object has no attribute 'SpatialReference'
# # >>> import osgeo.ogr
# # >>> srs = osgeo.ogr.SpatialReference()
# # Traceback (most recent call last):
# #   File "<stdin>", line 1, in <module>
# # AttributeError: '_Module' object has no attribute 'SpatialReference'
# # >>> srs = osgeo.osr.SpatialReference()
# # >>> srs.ImportFromWkt(geo.GetProjection())
# # 0
# # >>> srs
# # <osgeo.osr.SpatialReference; proxy of <Swig Object of type 'OSRSpatialReferenceS
# # hadow *' at 0x00000000021C9360> >
# # >>> srs.IsProjected()
# # 0
# # >>> srs.IsGeographic()
# # 1
# # >>> (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = geo.G
# # etGeoTransform()
# # >>> upper_left_x
# # -105.00055555489
# # >>> x_size
# # 9.2592592525846e-05
# # >>> x_rotation
# # 0.0
# # >>> y_rotation
# # 0.0
# # >>> arr.shape
# # (10812L, 10812L)
# # >>> upper_right_x = upper_left_x + x_size * arr.shape[0]
# # >>> upper right_x
# #   File "<stdin>", line 1
# #     upper right_x
# #                 ^
# # SyntaxError: invalid syntax
# # >>> upper_right_x
# # -103.99944444450055
