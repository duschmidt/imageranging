import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
def convertMask():
	#rangeFile='./GrandCanyon1/arrays/reduced.npy'
	rangeFile='./GrandCanyon2/arrays/range.obsLat_36.06608900.obsLon_-112.11761800.obsElev_2159.000.horizAngle_44.000.vertAngle_20.000.elevAngle_-4.000.heading_303.000.height_600.width_900.4_final.npy'
	r = np.load(rangeFile)
	f = open('./GrandCanyon2/GrandCanyonMask_900x600_2.txt','w')

	for row in range(r.shape[0]):
		for col in range(r.shape[1]):
			f.write("%d,"%r[row,col])
		f.seek(-1,1)
		f.write("\n")

	f.close()

#convertMask()
def showMask():
	f = open('./GrandCanyon2/GrandCanyonMask_900x600_2.txt','r')
	l = f.readline()
	n = np.zeros((600,900))
	row = 0

	while l != '':
		ls = l.split(',')
		for d in range(len(ls)):
			n[row, d] = ls[d]
		l = f.readline()
		row += 1
	mpimg.imsave("./GrandCanyon2/GrandCanyonMask_900x600_2.jpg", n)
	plt.imshow(n)
	plt.show()

showMask()